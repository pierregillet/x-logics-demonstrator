#include "formula.h"

#include <iostream>

void client::tab(int indent) {
    for (int i = 0; i < indent; ++i)
        std::cout << ' ';
}

void client::logic_node_printer::operator()(const std::string & text) const {
    tab(indent + tabsize);
    std::cout << "proposition: \"" << text << '"' << std::endl;
}

void client::logic_node_printer::operator()(const logic_tree_binary_node & node) const {
    std::string relation_literal;
    for (const auto &translation : client::operators_translation) {
        if (translation.second == node.relation) {
            relation_literal = translation.first;
            break;
        }
    }

    tab(indent);
    std::cout << '{' << std::endl;
    boost::apply_visitor(logic_node_printer(indent + tabsize), node.left);
    tab(indent);
//    std::cout << "        relation: " << node.relation << std::endl;
    std::cout << "        relation: " << relation_literal << std::endl;
//    tab(indent);
    boost::apply_visitor(logic_node_printer(indent + tabsize), node.right);
    tab(indent);
    std::cout << '}' << std::endl;
}

void client::logic_node_printer::operator()(const logic_tree_unary_node & node) const {
    std::string relation_literal;
    for (const auto &translation : client::operators_translation) {
        if (translation.second == node.relation) {
            relation_literal = translation.first;
            break;
        }
    }

    tab(indent);
    std::cout << '{' << std::endl;
    tab(indent);
//    std::cout << "        relation: " << node.relation << std::endl;
    std::cout << "        relation: " << relation_literal << std::endl;
//    tab(indent);
    boost::apply_visitor(logic_node_printer(indent + tabsize), node.child);
    tab(indent);
    std::cout << '}' << std::endl;
}
