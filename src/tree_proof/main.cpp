#include <iostream>
#include <string>

#include "parser.hpp"

int main() {
    namespace ascii = boost::spirit::ascii;
    namespace qi = boost::spirit::qi;

    std::cout << "/////////////////////////////////////////////////////////\n\n";
    std::cout << "\t\tA parser for propositional logic...\n\n";
    std::cout << "/////////////////////////////////////////////////////////\n\n";

    std::cout << "Give me a comma separated list of numbers.\n";
    std::cout << "Type [q or Q] to quit\n\n";

    client::propositional_logic_grammar<std::string::const_iterator> logic; // Our grammar
    // client::logic_tree ast; // Our tree

    std::string str;
    while (getline(std::cin, str)) {
        if (str.empty() || str[0] == 'q' || str[0] == 'Q') {
            break;
        }

        std::string::const_iterator iter = str.begin();
        std::string::const_iterator end = str.end();

        client::logic_node ast; // Our tree

//        client::logic_node test(
//              client::logic_tree_node{
//                    client::logic_node(std::string("a")),
////                    client::LogicOperator::AND,
//                    client::logic_node(std::string("c"))
//              }
//        );
//        boost::apply_visitor(client::logic_node_printer(), test);

        bool r = boost::spirit::qi::phrase_parse(iter, end, logic, ascii::space, ast);

        if (r && iter == end) {
            std::cout << "-------------------------\n";
            std::cout << "Parsing succeeded\n";
            std::cout << "-------------------------\n";

            boost::apply_visitor(client::logic_node_printer(), ast);
        } else {
            std::string::const_iterator some = iter+30;
            std::string context(iter, (some>end)?end:some);
            std::cout << "-------------------------\n";
            std::cout << "Parsing failed\n";
            std::cout << "stopped at: \": " << context << "...\"\n";
            std::cout << "-------------------------\n";
        }
    }

    return 0;
}
