#pragma once

#include <map>

#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/variant/recursive_variant.hpp>

namespace client {
    enum class LogicOperator {
        AND,
        OR,
        NOT
    };

    const std::map<std::string, LogicOperator> operators_translation{
          {"&", LogicOperator::AND},
          {"|", LogicOperator::OR},
          {"-", LogicOperator::NOT},
    };

    struct logic_tree_binary_node;
    struct logic_tree_unary_node;

    // WARNING : changing the order of the template types
    // of the following boost::variant will kill kittens.
    // And cause systematic segfaults. See :
    // https://stackoverflow.com/q/27871368/8394677
    // https://stackoverflow.com/questions/27871368/segmentation-fault-with-recursive-spirit-qi-grammar
    typedef boost::variant<
          std::string,
          boost::recursive_wrapper<logic_tree_binary_node>,
          boost::recursive_wrapper<logic_tree_unary_node>>
          logic_node;

    struct logic_tree_binary_node {
        logic_node left;
        LogicOperator relation;
        logic_node right;
    };

    struct logic_tree_unary_node {
        LogicOperator relation;
        logic_node child;
    };
}

// We need to tell fusion about our formula struct
// to make it a first-class fusion citizen
BOOST_FUSION_ADAPT_STRUCT(
      client::logic_tree_binary_node,
      (client::logic_node, left)
      (client::LogicOperator, relation)
      (client::logic_node, right)
)

BOOST_FUSION_ADAPT_STRUCT(
      client::logic_tree_unary_node,
      (client::LogicOperator, relation)
      (client::logic_node, child)
)

namespace client {
    int const tabsize = 4;

    void tab(int indent);

    struct logic_node_printer : public boost::static_visitor<> {
        int indent;

        explicit logic_node_printer(int indent = 0)
            : indent(indent) {
        }
        void operator()(const std::string & text) const;
        void operator()(const logic_tree_binary_node & node) const;
        void operator()(const logic_tree_unary_node & node) const;
    };
}