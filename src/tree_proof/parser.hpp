#pragma once
#define BOOST_SPIRIT_DEBUG

#include <boost/spirit/include/qi.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/variant/recursive_variant.hpp>

#include "formula.h"

namespace client
{
  namespace qi = boost::spirit::qi;
  namespace ascii = boost::spirit::ascii;
  namespace phoenix = boost::phoenix;

  template <typename Iterator>
  struct propositional_logic_grammar
      : qi::grammar<Iterator, logic_node(), ascii::space_type>
  {
    propositional_logic_grammar()
        : propositional_logic_grammar::base_type(formula)
    {
//            using qi::debug;

      formula %= formulaL1;

      formulaL1 %= formulaL2 >> disjunction_symbols_ >> formulaL1
                   | formulaL2;

      formulaL2 %= formulaL3 >> conjunction_symbols_ >> formulaL2
                   | formulaL3;

      formulaL3 %= negation_symbols_ >> formulaL4
                   | formulaL4;

      formulaL4 %= qi::lexeme[+(qi::alnum)]
                   | qi::lit("(") >> formula >> qi::lit(")");

//            BOOST_SPIRIT_DEBUG_NODE(formula);
    }

    const struct LogicOperator_ : qi::symbols<char, LogicOperator> {
      LogicOperator_() {
        for (const auto & translation : operators_translation) {
          this->add(translation.first, translation.second);
        }
      }
    } logic_operator_;

    const struct ConjunctionSymbols_ : qi::symbols<char, LogicOperator> {
      ConjunctionSymbols_() {
        this->add("&", LogicOperator::AND);
      }
    } conjunction_symbols_;

    const struct DisjunctionSymbols_ : qi::symbols<char, LogicOperator> {
      DisjunctionSymbols_() {
        this->add("|", LogicOperator::OR);
      }
    } disjunction_symbols_;

    const struct NegationSymbols_ : qi::symbols<char, LogicOperator> {
      NegationSymbols_() {
        this->add("-", LogicOperator::NOT);
      }
    } negation_symbols_;

  private:
    qi::rule<Iterator, logic_node(), ascii::space_type> formula;
    qi::rule<Iterator, logic_node(), ascii::space_type> formulaL1;
    qi::rule<Iterator, logic_node(), ascii::space_type> formulaL2;
    qi::rule<Iterator, logic_node(), ascii::space_type> formulaL3;
    qi::rule<Iterator, logic_node(), ascii::space_type> formulaL4;
  };
}
