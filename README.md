# Build #
## GNU/Linux ##
At the project root, run:
```
mkdir build
cd build
cmake ..
cmake --build .
```
And you should be able to find the executables in the build directory.